/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.shapeabstract;

/**
 *
 * @author HP
 */
public class Square extends Shape{
    private double s;
    
    public Square(double s) {
        super("Square");
        this.s = s;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }

    @Override
    public double calArea() {
        return s*s;
    }

    @Override
    public String toString() {
        return "Square{" + "s=" + s + '}';
    }
    
    
    
}
